import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Login from './components/Login';

import Error from './components/Error';
import Header from './components/shared/header'
import patientProfile from './components/patientProfile';
import PrivateRoute from './components/shared/PrivateRoute';
import noteFinal from './components/notes/patientNotes_final';
import allpatients from './components/AllPatients';
import allHistory from './components/history/AllHistory';
import CareproviderProfile from './components/CareproviderProfile';
import CareproviderNew from './components/CareproviderNew';
import DashboardStart from './components/Dashboard_A'
import AdminLogin from './components/AdminLogin';
import AdminHome from './components/AdminHome';
import CreateNew from './components/CreateNew';

function App() {
  return (
    <div className="wrapper">
      <BrowserRouter>
      <Header/>
     
        <Switch>
        <Route path="/" component={Login} exact/>
        {/* <Route path="/login" component={Login}/> */}
        <Route path="/noteFinal"component={noteFinal}/>
        <Route exact path="/patientProfile/:id" component= {patientProfile} />
        <Route path="/careproviderProfile"component={CareproviderProfile}/>
        <Route path="/dashboardstart"component={DashboardStart}/>
      
        <Route exact path="/patientProfile" component= {allpatients} />
        <Route exact path="/history" component= {allHistory} />

  
        <PrivateRoute path="/dashboard">
               <Dashboard />
      
              </PrivateRoute>
        <Route path="/createnew" component={CreateNew} />
        <Route path="/noteFinal"component={noteFinal}/>
        <Route exact path="/patientProfile/:id" component= {patientProfile} />
        <Route path="/provider/:id" component={CareproviderNew} exact/>
        <Route path="/admin_login" component={AdminLogin}/>
       < PrivateRoute path="/dashboard">
               <Dashboard />
      
              </PrivateRoute>
        <Route path="/createnew" component={CreateNew} />
        <Route path="/noteFinal"component={noteFinal}/>
        <Route exact path="/patientProfile/:id" component= {patientProfile} />
        <Route path="/provider/:id" component={CareproviderNew} exact/>
        <Route path="/admin_login" component={AdminLogin}/>
        <PrivateRoute path="/AdminHome">
             <AdminHome />
             </PrivateRoute>
            <PrivateRoute path="/dashboard">
             <Dashboard/>
             </PrivateRoute> 
        <Route component={Error}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
