import React, {useState} from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'

import CareproviderProfile from './CareproviderProfile';


const AdminHome = () => {
    let history = useHistory();
    const [id, setId] = useState("");
    const [patientSearchInput, setPatientSearchInput] = useState("");
    const [provider, setProvider] = useState({
        id: "",
        firstName: "",
        lastName: "",
        profession: "",
        address: "",
        phone: "",
        username:"",
        userpassword: ""
    });
    const [patient, setPatient] = useState({
        id: "",
        firstName: "",
        lastName: "",
        DOB: "",
        address: "",
        phone: ""
    });

    const [found, setFound] = useState(false);
    const [patientfound, setPatientFound] = useState(false);

    const [form, setForm] = useState({display: "none"});

    /*const [newProvider, setNewProvider] = useState({
        firstName:"",
        lastName:"",
        profession:"",
        address:"",
        phone:""
    })*/

    const handleChange = e => {
        e.preventDefault();
        setId(e.target.value)
    }

    const handlePatientChange = e => {
        e.preventDefault();
        setPatientSearchInput(e.target.value)
    }

    async function fetchPatient(patientSearchInput) {
        let id = patientSearchInput;
        const res = await fetch(`http://localhost:4000/patient_results/${id}`);
        res
            .json()
            .then((res) => setPatient(res))
            .catch((err) => console.error(err))
    }

    const submitPatientSearch = (e, patientSearchInput) => {
        e.preventDefault();
        fetchPatient(patientSearchInput);
        setPatientFound(true);
    }
    
    const submitSearch = (e, id) => {
        e.preventDefault();
        fetchProvider(id);
        setFound(true);
    }
    
    async function fetchProvider(id) {
        //let id = searchInput;
        const res = await fetch(`http://localhost:4000/api/provider/${id}`);
        //if (res == undefined) {
        //    console.log("no record found belonging to that id")
        //}
        res
            .json()
            .then((res) => setProvider(res))
            .catch((err) => console.error(err))
          
    }

    const handleNavigate = e => {
        e.preventDefault()
        history.push("/createnew")
    }

    const handleDelete = (e, provider) => {
        e.preventDefault();
        let id = provider.id;
        fetch(`/api/provider/${id}`, {
            method: "delete",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json"
            },
            body: JSON.stringify(provider)
        }).then((response) => response.json());
        history.push('/adminhome')
    }

    const handleEdit = (e, provider) => {
        e.preventDefault();
        setForm({display: "block"})
        //setProvider(provider);
    }

    const handleCancel = (e) => {
        e.preventDefault();
        setForm({display: "none"});
    }

    const handleFormChange = (e) => {
        setProvider((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value,
        }))
    };

    const handleFormSubmit = (e, provider) => {
        e.preventDefault();
        let id = provider.id;
        fetch(`/api/provider/${id}`, {
            method: "put",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json"
            },
            body: JSON.stringify(provider)
        }).then((response) => response.json());
        history.push('/adminhome')
    }

    const logout = e => {
        e.preventDefault()
        sessionStorage.removeItem('token')
        history.push("/adminlogin")
    }

    const { path, url } = useRouteMatch()

    return (
        <>
            <div>
                <header className="header-container">
                <h1>Admin Home</h1>
                </header>
            </div>

            <div className="flex-rowrev-cont">
                <button className="btn-green" onClick={logout}>Logout</button>
            </div>

            <div className="flex-container">
                <button className="btn-cyan" onClick={handleNavigate}>Create New Record</button>
                <h2>or</h2>
            </div>

            <div class="flex-container">
                <h3>Search for Patient:</h3>
                <input type="text" className="search-input" id="healthcard" placeholder="Enter Health Card Number" value={patientSearchInput} onChange={handlePatientChange}/>  
                <button className="btn-cyan" onClick={submitPatientSearch}>Search</button>
            </div>
            
            <div class="flex-container">
                <h3>Search for Health Care Provider:</h3>
                <input type="text" className="search-input" id="providerid" value={id} placeholder="Enter Provider ID number" onChange={handleChange}/>  
                <button className="btn-cyan" onClick={submitSearch}>Search</button>
            </div>

        <div style={{display: found ? "block" : "none"}}>
            <div className="flex-rowrev-cont">
                <button className="btn-red" onClick={handleDelete}>Delete Record</button>
                <button className="btn-green" onClick={handleEdit}>Edit</button>
            </div>
            <div className="flex-row-cont">
                <div className="flex-block">
                <h4>First Name</h4>
                <p>{provider.firstName}</p>
                </div>
                <div className="flex-block">
                <h4>Last Name</h4>
                <p>{provider.lastName}</p>
                </div>
                <div className="flex-block">
                <h4>Profession</h4>
                <p>{provider.profession}</p>
                </div>
                <div className="flex-block">
                <h4>Address</h4>
                <p>{provider.address}</p>
                </div>
                <div className="flex-block">
                <h4>Telephone</h4>
                <p>{provider.phone}</p>
                </div>
                
            </div>
            <form name="providerForm" style = {form} onSubmit={handleFormSubmit}>
                    <div className="form-container" className="flex-container">
                        <p class="form-headings">First Name:</p>
                            <input type="text"  name="firstName" className="form-inputs" required value={provider.firstName} onChange={handleFormChange}/>
                        <p class="form-headings">Last Name:</p>
                            <input type="text" name="lastName" className="form-inputs" required value={provider.lastName} onChange={handleFormChange}/>

                        <p class="form-headings">Profession:</p>
                            <input type="text" name="profession" className="form-inputs" required value={provider.profession} onChange={handleFormChange}/>
                            
                        <p class="form-headings">Address:</p>
                            <input type="address" name="address" className="form-inputs" required value={provider.address} onChange={handleFormChange}/>
                        <p class="form-headings">Phone:</p>
                            <input type="tel" name="phone" className="form-inputs" required value={provider.phone} /><br onChange={handleFormChange}/>  
                        <button className="btn-cyan" type="submit">Submit</button>{' '}
                        <button className="btn-red" onClick={handleCancel}>Cancel</button>
                    </div>
                    
                </form>
            
        </div>

            

    </>        
    )
}
export default AdminHome