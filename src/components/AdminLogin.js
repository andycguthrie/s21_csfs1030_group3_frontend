import React, {useState} from 'react'
import { Container, Card, CardBody, CardText } from 'reactstrap'
import { useHistory, useLocation } from 'react-router-dom'

const AdminLogin = () => {
    let history = useHistory();
    let location = useLocation();
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [auth, setAuth] = useState(true)

    const loginSubmit = async event => {
        
        event.preventDefault()
        const response = await fetch('http://localhost:4000/auth', {
            method: 'POST',
            url: "http://localhost:3000/adminhome",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({username, password})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            setAuth(false)
        } else {
            sessionStorage.setItem('token', payload.token)
            let { from } = location.state || { from: { pathname: "/dashboard" } };
            history.replace(from);
            history.push("/adminhome")
        }
    }
    return (
        <Container class="main-content">
        {!auth && 
            <Card className="text-white bg-primary my-5 py-4 text-center">
            <CardBody>
                <CardText className="text-white m-0">Invalid credentials, please try again</CardText>
            </CardBody>
        </Card>
        }
        <main>
            <header class="header-container">
                <h1>Log in as an Administrator</h1>
            </header>
            <div class="form-content">
                <form name="submitForm" onSubmit={loginSubmit}>
                    <div class="form-container">
                        <p class="form-headings">Username:</p>
                            <input type="text" id="username" class="form-inputs" required value={username} onChange={e => setUsername(e.target.value)}/>
                        <p class="form-headings">Password:</p>
                            <input type="password" id="password"class="form-inputs" required value={password} onChange={e => setPassword(e.target.value)}/>
                    </div>
                    <div>
                        <button>Sign In</button>
                    </div>
                </form>
            </div>
        </main>        
        </Container>
    )
}

export default AdminLogin