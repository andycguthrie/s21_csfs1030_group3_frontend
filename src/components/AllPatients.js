import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory } from "react-router-dom";

const AllPatients = (props) => {
  const [patients, setpatients] = useState([]);

  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/patient_results");
      res
        .json()
        .then((res) => setpatients(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  const patientProfileRoute = (event, patient) => {
    event.preventDefault();
    console.log(patient);
    // let path = `/patient_results/${patient.healthcard}`;
    history.push(`/patientprofile/${patient.healthcard}`)
  };

  return (
    
  <div className='main'>
       
      <Container >
      <h1 className="dark">All Patients</h1>
      {patients.map((patient) => (
        <div key={patient.healthcard}>
         
        <Row >
     
        <Col> <p>{patient.fname} {patient.lname}</p></Col> 
        <Col> <p>{patient.dob} {patient.dob}</p></Col> 
        <Col> <p>{patient.healthcard} </p></Col> 
       <Col> <Button onClick={(e) => patientProfileRoute(e, patient)}>
            View cat profile
          </Button></Col> 
        </Row>
        </div>
      ))}
      
     
      </Container>
      </div>
    
    
  );
};

export default AllPatients;
