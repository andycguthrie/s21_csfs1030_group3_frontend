import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory } from "react-router-dom";

const AllPatients = (props) => {
  const [patients, setpatients] = useState([]);

  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/patient_results");
      res
        .json()
        .then((res) => setpatients(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  const patientProfileRoute = (event, patient) => {
    event.preventDefault();
    console.log(patient);
    // let path = `/patient_results/${patient.healthcard}`;
    history.push(`/patientprofile/${patient.healthcard}`)
  };

  return (
    
    <Container className='main'>
    <h1>Portfolio</h1>
    {patients.map((patientsObject)=>(
        <div key={patientsObject}>
  <Row className="my-5">
     <Col lg="7">
     <img src={ppatientsObject.fname} />
     </Col>
     <Col lg="5">
     <h1 onClick={(e) => PortfolioRoute(e, portfolioObject)} className="xlarge-pages"> {portfolioObject.client}</h1>

</Col>
</Row>



 

        </div>
    ))}
</Container>
)
}


export default AllPatients;
