import React, {useState} from "react";
import { useHistory } from "react-router";


const CareProviderNew = () => {

    let history = useHistory();

    const [values, setValues] = useState({
        firstName: "",
        lastName:"",
        profession: "",
        address: "",
        phone: ""
    });

    const handleFirstNameChange = e => {
        setValues({...values, firstName: e.target.value})
    }
    const handleLastNameChange = e => {
        setValues({...values, lastName: e.target.value})
    }
    const handleProfessionChange = e => {
        setValues({...values, profession: e.target.value})
    }
    const handleAddressChange = e => {
        setValues({...values, address: e.target.value})
    }
    const handlePhoneChange = e => {
        setValues({...values, phone: e.target.value})
    }

    const clearState = () => {
        setValues({...values});
    };


    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/adminhome/careprovidernew', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(values)
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid}`)
        } else {
            alert(`Thank you! File has been created with id: ${payload.id}`);
        }
        clearState();
        history.push("/adminhome");
    }

    return (
        <>
            <div className="flex-container">
            <h1>Health Care Provider Form</h1>
            </div>

            <div class="form-content">
                <form name="newProviderForm">
                    <div class="form-container" onSubmit={formSubmit}>
                        <p class="form-headings">First Name:</p>
                            <input type="text" id="name" class="form-inputs" required value={values.firstName} onChange={handleFirstNameChange}/>
                        <p class="form-headings">Last Name:</p>
                            <input type="text" id="name" class="form-inputs" required value={values.lastName} onChange={handleLastNameChange}/>

                        <p class="form-headings">Profession:</p>
                            <input type="text" id="profession"class="form-inputs" required value={values.profession} onChange={handleProfessionChange}/>
                            
                        <p class="form-headings">Address:</p>
                            <input type="address" id="address"class="form-inputs" required value={values.address} onChange={handleAddressChange}/>
                        <p class="form-headings">Phone:</p>
                            <input type="tel" id="phone"class="form-inputs" required value={values.phone} onChange={handlePhoneChange}/>   
                    </div>
                    <div>
                        <button className="btn-cyan" type="submit">Submit</button>{' '}
                    </div>
                </form>
            </div>
        </>
    )

}

export default CareProviderNew