import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router";

const CareproviderProfile = () => {
    const { id } = useParams();

    const [provider, setProvider] = useState({});
    const [isEditing, setEditing] = useState(false);

    const [newFName, setNewFName] = useState("");
    const [newLName, setNewLName] = useState("");
    const [newProfession, setNewProfession] = useState("");
    const [newAddress, setNewAddress] = useState("");
    const [newPhone, setNewPhone] = useState("");

    const newValues = [];


    useEffect(() => {
        async function fetchData() {
            const res = await fetch(`http://localhost:4000/provider/${id}`,
            { method: "get",
            headers: {'Accept': 'application/json',
            'Content-Type': 'application/json'}
        })

            res
                .json
                .then((res) => setProvider(res))
                .catch((err) => console.log(err));
        }
        fetchData();
    }, [id]);

    const handleFNameChange = (e) => {
        setNewFName(e.target.value);
        if (newFName) {newValues.push(newFName)}
        else {return}
    }

    const handleLNameChange = (e) => {
        setNewLName(e.target.value);
        if (newLName) {newValues.push(newLName)}
        else {return}
    }

    const handleProfessionChange = (e) => {
        setNewProfession(e.target.value);
        if (newProfession) {newValues.push(newProfession)}
        else {return}
    }

    const handleAddressChange = (e) => {
        setNewAddress(e.target.value);
        if (newAddress) {newValues.push(newAddress)}
        else {return}
    }

    const handlePhoneChange = (e) => {
        setNewPhone(e.target.value);
        if (newPhone) {newValues.push(newPhone)}
        else {return}
    }

    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/provider/:id', {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(newValues)
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid}`)
        } else {
            alert(`Thank you! File has been created with id: ${payload.id}`);
        }
        setEditing(false);
    }

    let history = useHistory();

    const logout = e => {
        e.preventDefault()
        sessionStorage.removeItem('token')
        history.push("/adminlogin")
    };

    const viewTemplate = (
        <>
            <div className="flex-rowrev-cont">
                <button className="btn-green" onClick={logout}>Logout</button>
                <button className="btn-green" onClick={()=>setEditing(true)}>Edit</button>
            </div>

            {provider.map((provider) => (
            <div key={provider.id} className="flex-container">
                <p>First Name</p>
                <p>{provider.firstName}</p><br />
                <p>Last Name</p>
                <p>{provider.lastName}</p><br />
                <p>Profession</p>
                <p>{provider.profession}</p><br />
                <p>Address</p>
                <p>{provider.address}</p><br />
                <p>Telephone</p>
                <p>{provider.phone}</p>
            </div>))}
        </>
    )

    const editingTemplate = (
        <>
        <div className="flex-rowrev-cont">
                <button className="btn-green" onClick={logout}>Logout</button>
                <button className="btn-green" onClick={()=>setEditing(false)}>Cancel</button>
        </div>

        <div class="form-content">
                <form name="newProviderForm">
                    <div class="form-container" onSubmit={formSubmit}>
                        <p class="form-headings">First Name:</p>
                            <input type="text" id="name" class="form-inputs" required value={provider.firstName} onChange={handleFNameChange}/>
                        <p class="form-headings">Last Name:</p>
                            <input type="text" id="name" class="form-inputs" required value={provider.lastName} onChange={handleLNameChange}/>

                        <p class="form-headings">Profession:</p>
                            <input type="text" id="profession"class="form-inputs" required value={provider.profession} onChange={handleProfessionChange}/>
                            
                        <p class="form-headings">Address:</p>
                            <input type="address" id="address"class="form-inputs" required value={provider.address} onChange={handleAddressChange}/>
                        <p class="form-headings">Phone:</p>
                            <input type="tel" id="phone"class="form-inputs" required value={provider.phone} onChange={handlePhoneChange}/>   
                    </div>
                    <div>
                        <button className="btn-cyan" type="submit">Submit</button>{' '}
                    </div>
                </form>
            </div>
        
        </>
    )

    return (
        <>{isEditing ? editingTemplate : viewTemplate}</>
        
    )
}
export default CareproviderProfile