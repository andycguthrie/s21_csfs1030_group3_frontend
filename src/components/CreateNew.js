import React, { useState, useEffect } from "react";
import { Switch, Route, Link, useRouteMatch, useHistory } from "react-router-dom";
import PatientNew from './PatientNew';
import CareProviderNew from './CareproviderNew';

const CreateNew = () => {

    const { path, url } = useRouteMatch()
    const history = useHistory()

    const logout = e => {
        e.preventDefault()
        sessionStorage.removeItem('token')
        history.push("/adminlogin")
    };

    return (
        <>
            <div className="flex-rowrev-cont">
            
                <button className="btn-green" onClick={logout}>Logout</button>
                <button className="btn-green" onClick={()=>history.push("/adminhome")}>Cancel</button>
            </div>
            <div className="flex-container">
                <h1>Create new profile for</h1>
                
                <button className="btn-blue-border"><Link to={`${url}/patientnew`}>Patient</Link></button>
                
                <button className="btn-blue-border"><Link to={`${url}/careprovidernew`}>Provider</Link></button>
                
            </div>

            <Switch>
                <Route path={`${path}/patientnew`} component={PatientNew}></Route>
                <Route path={`${path}/careprovidernew`} component={CareProviderNew}></Route>
            </Switch>
        </>
    )
}
export default CreateNew