import React from 'react';
import { Form, FormGroup, Col, Input, Label, Button, Container, Row } from 'reactstrap';
import { useHistory } from 'react-router-dom';
// import Img from './images/Logo.png';
import Img from './images/Logo.png';
import Search from './Search';
import PatientProfile from './patientProfileXX';

 
const Dashboard = () => {
    let history = useHistory();

    const logout = e => {
        e.preventDefault()
        sessionStorage.removeItem('token')
        history.push("/login")
    }

    return (
       <div>
            {/* <header className="header-dashboard">
                <img className="logo" src={Img} alt="logo"/>
                <h1>Electronic Medical Record</h1>
            </header>  */}
        <div>
            <div className="search-container">
            </div>
            <div>
                <Search/>
            </div>
            </div>
            <div>
            </div>
            <div>
            <button className="button" onClick={logout}>Logout</button>
        </div>
        </div>
        
    );
}
 
export default Dashboard;