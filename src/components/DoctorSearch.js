import { useState, useEffect } from 'react';

const DoctorSearch = () => {
  const [text, setText] = useState("");
  const [data, setData] = useState([]); 
  const [doctor, setDoctor]  = useState([]);
  
useEffect(() => {
    async function fetchData() {
        const result = await fetch("http://localhost:4000/doctor_results");
        result
        .json()
        .then((result) => setData(result))
        .catch((err) => console.log(err));
    }
    fetchData();
}, []);

  const onSubmit = evt => {
    evt.preventDefault();
    if (text === "") {
      alert("Please enter doctor ID!");
    } else {
     data.filter(doctor => {
         if (doctor.doctor_id.includes(text)) {
            setDoctor(doctor)         
            }
            return false
          })
            setText("");
    }
   }

  const onChange = evt => setText(evt.target.value);

  return (
    <div>
      <div>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          name="text"
          placeholder="search doctor id..."
          value={text}
          onChange={onChange}
          className="search-input"
        />
        <button type="submit">
          Search
        </button>
      </form>
      </div>

      <div className="info-container">
        <ul>
         <li>Name: {doctor.fname} {doctor.lname} </li>
         <li>Username: {doctor.username}</li>
         <li>Address: {doctor.address}</li>
         <li>Phone Number: {doctor.phone}</li>
        </ul>
      </div>

    </div>
  );
};
 
export default DoctorSearch;