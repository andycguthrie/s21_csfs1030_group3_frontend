import React, { useState } from 'react'
import { Container, Card, CardBody, CardText } from 'reactstrap'
import { useHistory, useLocation, NavLink} from 'react-router-dom'
import Img from './images/Logo.png'

const Login = () => {
    let history = useHistory();
    let location = useLocation();
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [auth, setAuth] = useState(true)

    const loginSubmit = async event => {
        
        event.preventDefault()
        const response = await fetch('http://localhost:4000/auth', {
            method: 'POST',
            url: "http://localhost:4000/dashboard",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({username, password})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            setAuth(false)
        } else {
            sessionStorage.setItem('token', payload.token)
            let { from } = location.state || { from: { pathname: "/dashboardstart" } };
            history.replace(from);
        }
    }

    return (
        <Container className="main-content">
        {!auth && 
            <Card className="text-white bg-primary my-5 py-4 text-center">
            <CardBody>
                <CardText className="text-white m-0">Invalid credentials, please try again</CardText>
            </CardBody>
        </Card>
        }
        <main>
            <header className="header-container">
                <img className="logo" src={Img} alt="logo"/>
                <h1>Electronic Medical Record</h1>
            </header>
            <div className="form-content">
                <form name="submitForm" onSubmit={loginSubmit}>
                    <div className="form-container">
                        <p className="form-headings">Username:</p>
                            <input type="text" id="username" className="form-inputs" required value={username} onChange={e => setUsername(e.target.value)}/>
                        <p className="form-headings">Password:</p>
                            <input type="password" id="password"className="form-inputs" required value={password} onChange={e => setPassword(e.target.value)}/>
                    </div>
                    <div>
                        <button>Sign In</button>
                    </div>
                    <div className="align-right">
                    <NavLink className="admin-link" to="/admin_login">Admin Login</NavLink>
                    </div>
                </form>
            </div>
        </main>        
        </Container>
    )
}

export default Login