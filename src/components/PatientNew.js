import React, {useState} from "react";
import { useHistory } from "react-router";


const PatientNew = () => {

    let history = useHistory();

    const [newPatient, setNewPatient] = useState({
        healthcard: "",
        firstName: "",
        lastName:"",
        dob: "",
        address: "",
        phone: "",
       
    });

    const handleChange = (e) => {
        setNewPatient((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value,
        }));
    };

   /* const handleFirstNameChange = e => {
        setValues({...values, firstName: e.target.value})
    }
    const handleLastNameChange = e => {
        setValues({...values, lastName: e.target.value})
    }
    const handleDobChange = e => {
        setValues({...values, dob: e.target.value})
    }
    const handleAddressChange = e => {
        setValues({...values, address: e.target.value})
    }
    const handlePhoneChange = e => {
        setValues({...values, phone: e.target.value})
    }
    const handleHealthCardNumberChange = e => {
        setValues({...values, healthcard: e.target.value})
    }

    const clearState = () => {
        setValues({...values});
    };*/


    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/api/patientnew', {
            method: "post",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(newPatient),
        }).then((response) => response.json());
        //clearState();
        history.push("/adminhome");
    }

    return (
        <>
            <div className="flex-container">
            <h1>Patient Form</h1>
            </div>

            <div class="form-content">
                <form name="newPatientForm" onSubmit={formSubmit}>
                    <div class="form-container" >
                        <p class="form-headings">Health Card Number:</p>
                            <input type="text" name="healthcard"class="form-inputs" required value={newPatient.healthcard} onChange={handleChange}/>
                        <p class="form-headings">First Name:</p>
                            <input type="text" name="firstName" class="form-inputs" required value={newPatient.firstName} onChange={handleChange}/>
                        <p class="form-headings">Last Name:</p>
                            <input type="text" name="lastName" class="form-inputs" required value={newPatient.lastName} onChange={handleChange}/>

                        <p class="form-headings">Date of Birth:</p>
                            <input type="date" name="dob"class="form-inputs" required value={newPatient.dob} onChange={handleChange}/>
                            
                        <p class="form-headings">Address:</p>
                            <input type="address" name="address"class="form-inputs" required value={newPatient.address} onChange={handleChange}/>
                        <p class="form-headings">Phone:</p>
                            <input type="tel" name="phone"class="form-inputs" required value={newPatient.phone} onChange={handleChange}/>   
                        
                    </div>
                    <div>
                        <button className="btn-cyan" type="submit">Submit</button>{' '}
                    </div>
                </form>
            </div>
        </>
    )

}

export default PatientNew