import { useState, useEffect } from 'react';

const Search = () => {
  const [text, setText] = useState("");
  const [data, setData] = useState([]); 
  const [patient, setPatient]  = useState([]);
  
useEffect(() => {
    async function fetchData() {
        const result = await fetch("http://localhost:4000/patient_results");
        result
        .json()
        .then((result) => setData(result))
        .catch((err) => console.log(err));
    }
    fetchData();
});

  const onSubmit = evt => {
    evt.preventDefault();
    if (text === "") {
      alert("Please enter health card number!");
    } else {
     data.filter(patient => {
         if (patient.healthcard.includes(text)) {
            setPatient(patient)         
            }
            return false
          })
            setText("");
    }
   }

  const onChange = evt => setText(evt.target.value);

  return (
    <div>
      <div>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          name="text"
          placeholder="search health card number..."
          value={text}
          onChange={onChange}
          className="search-input"
        />
        <button type="submit">
          Search
        </button>
      </form>
      </div>

      <div className="info-container">
        <ul>
         <li>Name: {patient.fname} {patient.lname} </li>
         <li>Date of Birth: {patient.dob}</li>
         <li>Address: {patient.address}</li>
         <li>Phone Number: {patient.phone}</li>
        </ul>
      </div>

    </div>
  );
};
 
export default Search;