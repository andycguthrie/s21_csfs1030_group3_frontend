import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory } from "react-router-dom";

const AllHistory = (props) => {
  const [patients, setpatients] = useState([]);

  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:4000/history");
      res
        .json()
        .then((res) => setpatients(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  const patientProfileRoute = (event, patient) => {
    event.preventDefault();
    console.log(patient);
    // let path = `/patient_history/${patient.historyid}`;
    history.push(`/patientprofile/${patient.historyid}`)
  };

  return (
    
  <div className='main'>
       
      <Container >
      <h1 className="dark">All Patients</h1>
      {patients.map((patient) => (
        <div key={patient.historyid}>
         
        <Row >
     
        <Col> <p>{patient.date}</p></Col> 
        <Col> <p>{patient.action} </p></Col> 
        <Col> <p>{patient.created_by} </p></Col> 
       <Col> <Button onClick={(e) => patientProfileRoute(e, patient)}>
            View profile
          </Button></Col> 
        </Row>
        </div>
      ))}
      
     
      </Container>
      </div>
    
    
  );
};

export default AllHistory;
