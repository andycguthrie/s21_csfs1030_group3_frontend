import React, { useState, useEffect, Fragment } from "react";
import { nanoid } from "nanoid";
import ReadOnlyRow from "./ReadOnlyRow";
import EditableRow from "./EditableRow";
import NewHistory from "./historyNew"



const PatientNoteFinal = () => {

  const [contacts, setContacts]=useState([])
    
  useEffect(() => {
      async function fetchData() {
        const res = await fetch("http://localhost:4000/history");
        res
          .json()
          .then((res) => setContacts(res))
          .catch((err) => console.log(err));
      }
      fetchData();
    }, []);

    
    const [addFormData, setAddFormData] = useState({
    date: "",
   note: "",
  });

  const [editFormData, setEditFormData] = useState({
    date: "",
    note: "",
  });

  const [editContactId, setEditContactId] = useState(null);

  const handleAddFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...addFormData };
    newFormData[fieldName] = fieldValue;

    setAddFormData(newFormData);
  };

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData);
  };

  const handleAddFormSubmit = (event) => {
    event.preventDefault();
   

    const newContact = {
    
      date: addFormData.date,
      action: addFormData.action,
      created_by: addFormData.created_by,
      
    };

    const newContacts = [...contacts, newContact];
    setContacts(newContacts);
  };

  const handleEditFormSubmit = (event) => {
    event.preventDefault();

    const editedContact = {
      
      action: editFormData.action,
      date: editFormData.date,
      created_by: editFormData.created_by,
    };

    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === editContactId);

    newContacts[index] = editedContact;

    setContacts(newContacts);
    setEditContactId(null);
  };

  const handleEditClick = (event, contact) => {
    event.preventDefault();
    setEditContactId(contact.id);

    const formValues = {
      Date: contact.Date,
      Note: contact.Note,
    
    };

    setEditFormData(formValues);
  };

  const handleCancelClick = () => {
    setEditContactId(null);
  };

  const handleDeleteClick = (contactId) => {
    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === contactId);

    newContacts.splice(index, 1);

    setContacts(newContacts);
  };

  return (
    <div >
       
      <h2>Patient History</h2>
  
    {contacts.map((contact) => (
       
     
   
    <ReadOnlyRow 
       contact={contact}
      
      />
     
     
      
    ))}
    
   
 
    <h2>Add History</h2>
    <NewHistory/>
    </div>
  
  
);
};

export default PatientNoteFinal;
