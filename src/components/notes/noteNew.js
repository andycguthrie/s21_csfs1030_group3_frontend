import React, { useState } from "react";
import { Container, Row, Col, Button, Form, Label, FormGroup, Input} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';


const AddNoteForm = () => {
  const [newNote, setNewNote] = useState({ date: "", content: "", created_by: "" });


  const handleChange = (event) => {
    setNewNote((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = () =>{
     fetch("http://localhost:4000/notes", {
       method: "post",
       headers: {
         Accept: "application/json",
         "Content-Type": "application/json",
       },

       //make sure to serialize your JSON body
       body: JSON.stringify(newNote),
     }).then((response) => response.json());
 
  }

  return (
    <div className="App">
    <Form className="form" onSubmit={handleSubmit}>
      <FormGroup>
        <Label>Date</Label>
        <Input
         type="date"
         name="date"
         onChange={handleChange}
        />
      </FormGroup>
      <FormGroup>
        <Label>Note</Label>
        <Input
           type="text"
           name="content"
           placeholder="Enter Note"
           onChange={handleChange}
          
        />
      </FormGroup>
      <FormGroup>
        <Label>Doctor</Label>
        <Input
           type="text"
           name="created_by"
           placeholder="Enter Doctors Name"
           onChange={handleChange}
          
        />
      </FormGroup>
    <Button>Submit</Button>
  </Form>
</div>
);
}

export default AddNoteForm;
