import React, { useState, useEffect, Fragment } from "react";
import { nanoid } from "nanoid";
import { Button, Modal, Container, Col, Row} from "reactstrap";
import ReadOnlyRow from "./ReadOnlyRow";
import NewNote from "./noteNew"


const PatientNoteFinal = () => {

  const [contacts, setContacts]=useState([])
    
  useEffect(() => {
      async function fetchData() {
        const res = await fetch("http://localhost:4000/notes");
        res
          .json()
          .then((res) => setContacts(res))
          .catch((err) => console.log(err));
      }
      fetchData();
    }, []);

    
    const [addFormData, setAddFormData] = useState({
    date: "",
   content: "",
   created_by: "",
  });

  const [editFormData, setEditFormData] = useState({
    date: "",
    content: "",
    created_by: "",
  });

  const [editContactId, setEditContactId] = useState(null);

  const handleAddFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...addFormData };
    newFormData[fieldName] = fieldValue;

    setAddFormData(newFormData);
  };

  const handleEditFormChange = (event) => {
    event.preventDefault();

    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    const newFormData = { ...editFormData };
    newFormData[fieldName] = fieldValue;

    setEditFormData(newFormData);
  };

  const handleAddFormSubmit = (event) => {
    event.preventDefault();
   

    const newContact = {
     
      date: addFormData.date,
      content: addFormData.content,
      created_by: addFormData.created_by,
      
    };

    const newContacts = [...contacts, newContact];
    setContacts(newContacts);
  };

  const handleEditFormSubmit = (event) => {
    event.preventDefault();

    const editedContact = {
      
      content: editFormData.content,
      date: editFormData.date,
      created_by: editFormData.created_by,
    };

    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === editContactId);

    newContacts[index] = editedContact;

    setContacts(newContacts);
    setEditContactId(null);
  };

  const handleEditClick = (event, contact) => {
    event.preventDefault();
    setEditContactId(contact.id);

    const formValues = {
      created_by: contact.created_by,
      content: contact.content,
      date: contact.date,
    
    };

    setEditFormData(formValues);
  };

  const handleCancelClick = () => {
    setEditContactId(null);
  };

  const handleDeleteClick = (contactId) => {
    const newContacts = [...contacts];

    const index = contacts.findIndex((contact) => contact.id === contactId);

    newContacts.splice(index, 1);

    setContacts(newContacts);
  };

  return (
    <div >
       <Button></Button>
       <h2>Patient Notes</h2>
  
      {contacts.map((contact) => (
         
       
     
      <ReadOnlyRow 
         contact={contact}
        
        />
       
       
        
      ))}
      
     
   
      <h2>Add Note</h2>
      <NewNote/>
      </div>
    
    
  );
};

export default PatientNoteFinal;
