import React, { useState, useEffect } from "react";
import { Container, Button, Row, Col} from 'reactstrap';
import { useParams } from "react-router";
import AllNotes from './notes/patientNotes_final';
import AllHistory from './history/patientHistory';
import PatientProfile from "./patientProfileXX";

const PatientItem = () => {
  const [patientProfile, setPatient] = useState([]);
  const { id } = useParams();

 
  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/patient_results/${id}`);
      res
        .json()
        .then((res) => setPatient(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [id]);
  return (

    <Container >
    
            <div className='main'> 
            <Row>
       <Col sm={2}  > 
     <PatientProfile/>
     </Col>

     <Col> <AllNotes/> </Col>     
     <Col> <AllHistory/> </Col>    
         
     </Row>

     </div>
     
        
    </Container>
    
  
  );
  



  
};
export default PatientItem;