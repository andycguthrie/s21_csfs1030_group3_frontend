import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit, faTrash, faUser, faAddressBook, faPhone, faAt } from '@fortawesome/free-solid-svg-icons'



const PatientProfile = () => {
  const { id } = useParams();
  const [profile, setProfile] = useState([]);


  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/patient_results/${id}`);
      res
        .json()
        .then((res) => setProfile (res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [id]);

 

  return (

   

    <div>

  
      {profile.map((Profile) => (
        <div key={Profile.id}>
          <h1 className='dark'><FontAwesomeIcon icon={faUser} /> </h1>
         <h1 className='dark'>{Profile.fname} {Profile.lname}</h1>
   <p><FontAwesomeIcon icon={faEdit} /> {Profile.dob}</p>
  <p> <FontAwesomeIcon icon={faAddressBook} /> {Profile.address}, {Profile.City}, {Profile.Province}</p>
  <p> <FontAwesomeIcon icon={faPhone} /> {Profile.phone}</p>
         
        </div>
      ))}

      
    </div>
  );
};

export default PatientProfile;
