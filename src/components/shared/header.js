import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container, Button } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'
import Img from '../images/Logo.png';


const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    return (
        <Container>
<header className="header-dashboard">
                <img className="logo" src={Img} alt="logo"/>
                <h1>Electronic Medical Record</h1>
          
            </header>
            <Button  href="/dashboardstart">Dashboard</Button>
            </Container>
      
    )
}

export default Navigation